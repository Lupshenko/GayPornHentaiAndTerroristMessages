function wget --wraps=wget
    command wget --hsts-file /dev/null $argv;
end

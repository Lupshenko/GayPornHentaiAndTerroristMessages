function less --wraps=less
    command less\
        --ignore-case\
        --RAW-CONTROL-CHARS\
        --quit-if-one-screen\
        $argv;
end

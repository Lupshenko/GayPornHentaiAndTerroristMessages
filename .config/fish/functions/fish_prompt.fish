function fish_prompt
    if test -e /run/.containerenv
        printf '📦 '
    end

    printf '%s%s%s@%s%s%s:%s%s%s$ '\
        (set_color yellow) (whoami) (set_color normal)\
        (set_color green) (prompt_hostname) (set_color normal)\
        (set_color cyan) (prompt_pwd) (set_color normal)
end

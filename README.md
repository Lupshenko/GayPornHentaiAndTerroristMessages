Dotfiles.

## How to use

1. Install Fedora Silverblue as usual
2. Set the hostname so it ends with `.home.arpa`
3. `curl -sSL https://www.bruh.ltd/leenoocks | sh`
4. Reboot
5. `,do-everything`
6. Reboot

## License

[Zero-Clause BSD](LICENSE).
